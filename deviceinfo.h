/***************************************************************************
**
** Copyright (C) 2020 AT-SMB Team
** Contact: vagiz@duseev.com
**
****************************************************************************/

#ifndef DEVICEINFO_H
#define DEVICEINFO_H

#include <QObject>
#include <qbluetoothdeviceinfo.h>
#include <qbluetoothaddress.h>
#include <QList>

class DeviceInfo: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString deviceName READ getName NOTIFY deviceChanged)
    Q_PROPERTY(QString deviceAddress READ getAddress NOTIFY deviceChanged)
    Q_PROPERTY(int deviceSignal READ getSignal NOTIFY deviceChanged)

public:
    DeviceInfo() = default;
    DeviceInfo(const QBluetoothDeviceInfo &d);

    QString getAddress() const;
    QString getName() const;
    int getSignal() const;

    QBluetoothDeviceInfo getDevice();
    void setDevice(const QBluetoothDeviceInfo &dev);

Q_SIGNALS:
    void deviceChanged();

private:
    QBluetoothDeviceInfo device;
};

#endif // DEVICEINFO_H
