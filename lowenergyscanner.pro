TARGET = safedistancepln
INCLUDEPATH += .

QT += quick bluetooth multimedia multimediawidgets

# Input
SOURCES += main.cpp \
    bluetoothscanner.cpp \
    deviceinfo.cpp

OTHER_FILES += assets/*.qml

HEADERS += \
    bluetoothscanner.h \
    deviceinfo.h

RESOURCES += \
    resources.qrc

target.path = $$[QT_INSTALL_EXAMPLES]/bluetooth/lowenergyscanner
INSTALLS += target
