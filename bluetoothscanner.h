/***************************************************************************
**
** Copyright (C) 2020 AT-SMB Team
** Contact: vagiz@duseev.com
**
****************************************************************************/

#ifndef BLUETOOTHSCANNER_H
#define BLUETOOTHSCANNER_H

#include <qbluetoothlocaldevice.h>
#include <qbluetoothdeviceinfo.h>
#include <qbluetoothaddress.h>
#include <QObject>
#include <QVariant>
#include <QList>
#include <QMap>
#include <QBluetoothDeviceDiscoveryAgent>
#include "deviceinfo.h"

QT_FORWARD_DECLARE_CLASS (QBluetoothDeviceInfo)

class BluetoothScanner: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant devicesList READ getDevices NOTIFY devicesUpdated)
    Q_PROPERTY(QString update READ getUpdate WRITE setUpdate NOTIFY updateChanged)
    Q_PROPERTY(bool getState READ getState NOTIFY stateChanged)
    Q_PROPERTY(bool isTooClose READ isTooClose NOTIFY isTooCloseChanged)

public:
    BluetoothScanner();
    ~BluetoothScanner();

    QVariant    getDevices();
    QString     getUpdate();
    bool        getState();
    bool        isTooClose();

public slots:
    void startDeviceDiscovery();

private slots:
    void addDevice(const QBluetoothDeviceInfo&);
    void deviceScanFinished();
    void deviceScanError(QBluetoothDeviceDiscoveryAgent::Error);

signals:
    void devicesUpdated();
    void updateChanged();
    void stateChanged();
    void isTooCloseChanged();

private:
    void setUpdate(const QString &message);

private:
    QBluetoothDeviceDiscoveryAgent *    m_discoveryAgent;
    QMap<QString, QObject *>            m_devices;
    QString                             m_message;
    bool                                m_isScanning = false;
    bool                                m_isTooClose = false;
};

#endif // BLUETOOTHSCANNER_H
