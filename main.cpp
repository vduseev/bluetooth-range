/***************************************************************************
**
** Copyright (C) 2020 AT-SMB Team
** Contact: vagiz@duseev.com
**
****************************************************************************/

//#include <QtCore/QLoggingCategory>
#include <QQmlContext>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include "bluetoothscanner.h"


int main(int argc, char *argv[])
{
    //QLoggingCategory::setFilterRules(QStringLiteral("qt.bluetooth* = true"));
    //QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    BluetoothScanner s;
//    QQmlApplicationEngine e;

//    e.rootContext()->setContextProperty("scanner", &s);
//    e.load(QUrl("qrc:/assets/main.qml"));

//    if (e.rootObjects().isEmpty()) {
//        return -1;
//    }

    auto view = new QQuickView;
    view->rootContext()->setContextProperty("scanner", &s);

    view->setSource(QUrl("qrc:/assets/main.qml"));
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();

    return app.exec();
}
