/***************************************************************************
**
** Copyright (C) 2020 AT-SMB Team
** Contact: vagiz@duseev.com
**
****************************************************************************/

import QtQuick 2.0
import QtMultimedia 5.14

Rectangle {
    id: back
    width: 300
    height: 600

    Component.onCompleted: {
        scanner.startDeviceDiscovery()
    }

    Connections {
        target: scanner
        onIsTooCloseChanged: {
            if (scanner.isTooClose) {
                alarm.play()
            }
        }
    }

    SoundEffect {
        id: alarm
        source: "alarm2.wav"
    }

    Rectangle {
        id: visualAlert

        anchors.centerIn: parent
        width: parent.width * 0.5
        height: parent.width * 0.5
        radius: parent.width * 0.25

        color: "red"
        opacity: 0.3

        //visible: scanner.isTooClose

        states: State {
            name: "alert"
            when: scanner.isTooClose
            PropertyChanges {
                target: visualAlert
                width: back.width * 0.8
                height: back.width * 0.8
                radius: back.width * 0.4
                opacity: 0.9
            }
        }

        transitions: Transition {
            NumberAnimation {
                properties: "width,height,radius,opacity"
                easing.type: Easing.InOutQuad
            }
        }

        Text {
            id: warningText

            //anchors.fill: parent
            anchors.centerIn: parent

            text: "TOO CLOSE"
            opacity: 0.0
            font.bold: true
            font.pixelSize: parent.height / 8
            color: "yellow"

            states: State {
                name: "warning"
                when: scanner.isTooClose
                PropertyChanges {
                    target: warningText
                    opacity: 1.0
                }
            }

            transitions: Transition {
                NumberAnimation {
                    properties: "opacity"
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }

    ListView {
        id: theListView

        anchors.fill: parent

        clip: true
        model: scanner.devicesList
        opacity: 0.2

        delegate: Rectangle {
            id: box

            height: 50
            width: parent.width

            color: "lightsteelblue"
            border.width: 2
            border.color: "black"
            radius: 5

            Component.onCompleted: {
                //info.visible = false;
                //header.headerText = "Select a device";
            }

            Column {
                id: deviceColumn

                anchors.fill: parent

                spacing: 2

                Text {
                    id: deviceName
                    text: modelData.deviceName + " @"
                    font.pointSize: 12
                }

                Text {
                    id: deviceAddress
                    text: modelData.deviceAddress
                    font.pointSize: 12
                }

                Text {
                    id: deviceSignal
                    text: modelData.deviceSignal
                    font.pointSize: 12
                }
            }
        }
    }
}
